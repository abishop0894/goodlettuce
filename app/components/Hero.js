import { Box, useMediaQuery, Typography } from "@mui/material";
import CtaBox from "./CtaBox";

import CustomButton from "./CustomButton";

const Hero = ({
  cta,
  title,
  children,
  height
}) => {

  return (
    <div className={`mt-[12vh] bg-[url('https://www.theartisttree.com/wp-content/uploads/2021/02/green-smoke.jpg')] ${height} flex items-center justify-center border-b-2 border-black overflow-hidden relative  object-cover`} id={"hero"}>
      {cta ? (
        <CtaBox title={title}>
          <CustomButton title={'Book  table'} />
          <CustomButton title={'Book a table'} />
        </CtaBox>
      ) : (
        <div

        >
          {children}
        </div>
      )}
    </div>
  );
};

export default Hero;
