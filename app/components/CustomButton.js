import Link from "next/link";

const CustomButton = ({ title }) => {

  return <button
    className='border w-[100%] px-8 bg-transparent py-2 hover:bg-[#d56060] border-[#FDFAEC] hover:border-transparent transition-colors ease-out'>
    <Link href='' className=" text-[#FDFAEC] uppercase  ">
      {title}
    </Link>
  </button>
};

export default CustomButton;
