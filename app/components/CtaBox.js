import { Stack, Typography } from "@mui/material";
import FadeIn from "../Utils/FadeIn";

const CtaBox = ({ children, title }) => {
  return (
    <Stack
      className="p-8 border-2  border-[#FDFAEC] flex items-center justify-center mx-12 column gap-4 lg:w-[60%]"
    >
      {/*adjust font per screen */}
      <FadeIn>
        {" "}
        <Typography
          color={"#FDFAEC"}
          fontWeight={"bolder"}
          variant="h4"
          textAlign={"center"}

        >
          {title}
        </Typography>{" "}
      </FadeIn>
      <div
        className="flex flex-col sm:flex-row gap-4 md:gap-8 items-center justify-center"
      >
        {children}
      </div>
    </Stack>
  );
};

export default CtaBox;
