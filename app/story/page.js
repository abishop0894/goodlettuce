
"use client";
import Page from "../components/Page";
import Hero from "../components/Hero";
import { Divider, Typography } from "@mui/material";
import FadeIn from "../Utils/FadeIn";
import Image from "next/image";

const StoryPage = ({ }) => {

    return (

        <Page>
            <Hero
                height={'h-[25vh]'}
                cta={false}
                backgroundImg={
                    "https://cdn.discordapp.com/attachments/1112569325795160115/1134860945630445718/logo_make_11_06_2023_222.jpg"
                }
                children={
                    <FadeIn delay={1}>
                        <Typography
                            display={"flex"}
                            alignSelf={"center"}
                            variant="h1"
                            color={"#FDFAEC"}
                        >
                            Story
                        </Typography>
                    </FadeIn>
                }
            />
            <main className="px-5 py-10 w-[100%] md:max-w-[1200px] mx-auto flex flex-col gap-10">

                <section className="order-1  py-20   px-5  flex flex-col md:flex-row gap-x-10 lg:items-start items-center justify-between ">


                    <div className=" h-[500px] md:w-[50%] w-[100%] relative order-2 ">
                        <Image
                            src='https://www.theartisttree.com/wp-content/webpc-passthru.php?src=https://www.theartisttree.com/wp-content/uploads/2021/12/our-story-the-artist-tree-dispensary.jpg&nocache=1'
                            objectFit="cover"
                            fill
                            layout='fill'
                            sizes='100vw'
                        />
                    </div>


                    <div className="lg:mb-0 mb-10 lg:w-[50%] w-[100%] lg:pt-0  order-1">
                        <h2 className="text-black font-bold text-5xl ">Our <br /> story</h2>
                        <Divider className="mt-[3vh] mb-8 bg-black w-20 " />
                        <p className="pb-5">
                            The Artist Tree delivers immersive experiences and one-of-a-kind destinations for cannabis, community, culture, and, dare we say – getting high.
                        </p>
                        <p className="pb-5">
                            Our founders have deep roots in the cannabis community and have operated licensed dispensaries for over 13 years. When we created The Artist Tree, we sought to shatter stereotypes surrounding cannabis consumption by creating welcoming, inclusive, and innovative venues that promote local arts culture.
                        </p>
                        <p className="">
                            Cannabis has been used as a source of creative inspiration by artists for thousands of years. And we all know that cannabis makes concerts, museums and pretty much everything, more fun! The Artist Tree locations highlight the natural synergy between the arts and cannabis. Our retail stores and lounges showcase revolving exhibits from local artists and a variety of arts programming.

                        </p>
                    </div>


                </section>

                <section className="order-3 lg:order-2  py-20  px-5   flex flex-col items-center  justify-between">

                    <h2 className=" text-black font-bold text-5xl text-center">meet our founders</h2>
                    <Divider className="mt-[3vh] mb-8 bg-black w-20 " />

                    <div className=" h-[400px]  w-[100%] relative">
                        <Image
                            src='https://www.theartisttree.com/wp-content/webpc-passthru.php?src=https://www.theartisttree.com/wp-content/uploads/2021/12/our-founders-the-artist-tree-dispensary.jpg&nocache=1'
                            objectFit="cover"

                            layout='fill'
                            sizes='100vw'
                        />
                    </div>
                    <Divider className="mt-[3vh] mb-8  w-0 " />

                    <div className="flex flex-wrap justify-center gap-x-20 gap-y-4">

                        <div className="flex flex-col gap-1 items-center">
                            <h2 className="text-black font-bold text-xl ">Person</h2>
                            <h3 className="text-black/60  text-lg">Role</h3>
                        </div>
                        <div className="flex flex-col gap-1 items-center">
                            <h2 className="text-black font-bold text-xl ">Person</h2>
                            <h3 className="text-black/60  text-lg">Role</h3>
                        </div>
                        <div className="flex flex-col gap-1 items-center">
                            <h2 className="text-black font-bold text-xl ">Person</h2>
                            <h3 className="text-black/60  text-lg">Role</h3>
                        </div>
                        <div className="flex flex-col gap-1 items-center">
                            <h2 className="text-black font-bold text-xl ">Person</h2>
                            <h3 className="text-black/60  text-lg">Role</h3>
                        </div>
                    </div>

                </section>

                <section className="order-2  py-20  px-5  flex flex-col md:flex-row gap-x-10 lg:items-start items-center justify-between ">

                    <div className=" order-2 h-[400px] lg:w-[50%] w-[100%] relative">
                        <Image
                            src='https://www.theartisttree.com/wp-content/webpc-passthru.php?src=https://www.theartisttree.com/wp-content/uploads/2021/12/our-story-2-the-artist-tree-dispensary.jpg&nocache=1/'
                            objectFit="cover"
                            fill
                            layout='fill'
                            sizes='100vw'
                        />
                    </div>

                    <div className=" lg:mb-0 mb-10 lg:w-[50%] w-[100%] lg:pt-0 order-1 md:order-2">
                        <h2 className="text-black font-bold text-5xl ">Our <br /> mission</h2>
                        <Divider className="mt-[3vh] mb-8 bg-black w-20 " />
                        <p className="pb-5">
                            The Artist Tree delivers immersive experiences and one-of-a-kind destinations for cannabis, community, culture, and, dare we say – getting high.
                        </p>

                        <p className="">
                            Cannabis has been used as a source of creative inspiration by artists for thousands of years. And we all know that cannabis makes concerts, museums and pretty much everything, more fun! The Artist Tree locations highlight the natural synergy between the arts and cannabis. Our retail stores and lounges showcase revolving exhibits from local artists and a variety of arts programming.

                        </p>
                    </div>

                </section>

                <section className=" order-4 py-20 flex flex-col md:flex-row items-center justify-center">
                    Goodlettuse &copy; 2023
                </section>

            </main>
        </Page>
    );
};

export default StoryPage;
