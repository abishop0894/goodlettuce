/** @type {import('next').NextConfig} */
const nextConfig = {
    images: {
        formats: ['image/avif', 'image/webp',],
        domains: ['www.theartisttree.com']
    }
}

module.exports = nextConfig
